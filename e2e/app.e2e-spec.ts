import { ExamDevinFEPage } from './app.po';

describe('exam-devin-fe App', () => {
  let page: ExamDevinFEPage;

  beforeEach(() => {
    page = new ExamDevinFEPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
