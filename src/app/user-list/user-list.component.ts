import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private api:APIService) { }

  ngOnInit() {
    
  }

  remove(index){
    this.api.uList.splice(index,1);
  }

}
