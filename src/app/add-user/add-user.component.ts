import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private api:APIService) { }

  bName:string = "";
  bEmail:string = "";
  bAddress:string= "";
  bPhone:number= null;
  bCompany:string="";
  id:number=null;
  ngOnInit() {
    this.id=this.api.uList.length+1
  }

   addBook(){
    this.api.uList.push({"id":this.id, "name":this.bName, "email":this.bEmail, "{{address.street}}":this.bAddress});
    this.bName = "";
    this.bEmail = "";
    this.bAddress = "";
    this.bPhone = null;
    this.bCompany ="";
  }

}
